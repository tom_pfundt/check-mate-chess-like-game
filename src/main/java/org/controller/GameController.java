package org.controller;

import org.CheckMate;
import org.model.*;
import org.view.*;

import static java.util.stream.IntStream.range;
import static org.CheckMate.newLogin;

public class GameController
{
    //Declare variables for the chessboard model object and the chessboard views
    private ChessBoard chessBoard;
    private ChessBoardView boardView;
    private final BoardCellView[][] cells;

    public GameController()
    {
        //initialise the chessboard passing a reference to this controller
        chessBoard = new ChessBoard();
        //Initialise the board cells view to the board size
        cells = new BoardCellView[chessBoard.getBoardSize()][chessBoard.getBoardSize()];
    }

    public ChessBoardView getBoardView()
    {
        return boardView;
    }

    public BoardCellView[][] getCells()
    {
        return cells;
    }

    public ChessBoard getChessBoard()
    {
        return chessBoard;
    }

    //Build board view - cannot be done until players have logged in
    public void buildBoardView()
    {
        boardView = new ChessBoardView(this);
    }

    //Prompt players notifying that the database connection has failed and players are set to default
    public void loginFailure()
    {
        PopUpView.message("Database Connection Unsuccessful. Players have been logged in as default users.", "Connection Failed");
    }

    //Select piece to move when it is the players turn, based on x,y co-ordinates attained by clicking on the chessboardView
    public void selectPiece(int x, int y)
    {
        //Check that it is the players turn and that they have not already selected a piece
        if (chessBoard.getBoard()[x][y] == chessBoard.getPlayersTurn())
        {
            Piece selectedPiece = chessBoard.getPieces()[x][y];

            //Set the selected piece to the piece they have selected
            chessBoard.setSelectedPiece(selectedPiece);

            //Check if the selected piece is currently merged with another piece
            if (selectedPiece.isMerged())
            {
                //If it is merged, prompt player if the wish to split the piece or make a move
                String selection = PopUpView.prompt(selectedPiece);

                //Check if value of selection is one of the merged pieces to un-merge else show possible piece moves
                if (selection != null && !selection.equals("Cancel"))
                {
                    //for each piece that the selected piece is merged with
                    for (Piece mergedPiece : selectedPiece.getMergedWith())
                    {
                        //check to see which of the merged pieces the selection from the pop up matches
                        if (selection.equals(mergedPiece.getClass().getSimpleName()))
                        {
                            //when the math is found, set unMerge status boolean to true and set the unMerge Piece to
                            //the specific piece the player chose to split, then break.
                            selectedPiece.setUnMerge(true);
                            selectedPiece.setUnMergePiece(mergedPiece);
                            break;
                        }
                    }
                }
            }
            //show possible moves
            showPossibleMoves(x, y, selectedPiece);
        }
    }

    //Method to display the possible moves for a given selected piece
    private void showPossibleMoves(int x, int y, Piece selectedPiece)
    {
        PlayerID[][] board = chessBoard.getBoard();
        //If the player has decided to un-merge a merged piece
        if (selectedPiece.isUnMerge())
        {
            //for each cell immediately adjacent to that of the selected piece, if it is vacant - board[][] = 0
            //highlight orange
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    try
                    {
                        if (board[x + i][y + j] == PlayerID.NO_PLAYER)
                        {
                            cells[x + i][y + j].highlightCell(Colour.ORANGE);
                        }
                    }
                    //Exception ignored as it is a move that is off the board
                    catch (ArrayIndexOutOfBoundsException ignored)
                    {
                    }
                }
            }
        }
        //if the player is not un-merging a piece
        else
        {
            //for each move that the selected piece has in its moves HashSet
            for (Move move : selectedPiece.getMoves())
            {
                //get the moves x & y co-ordinates
                int moveX = move.getX();
                int moveY = move.getY();
                try
                {
                    //If the selected piece is not an instance of knight, it cannot jump over other pieces
                    // && if there is either players piece in the directly adjacent cell
                    if (!(!(selectedPiece instanceof Knight) && (Math.abs(moveX) == 2 || Math.abs(moveY) == 2) && board[x + (moveX / 2)][y + (moveY / 2)] != PlayerID.NO_PLAYER))
                    {
                        // for each move, if the move from the selected pieces position is not assigned to the current player
                        if (board[x + moveX][y + moveY] != chessBoard.getPlayersTurn())
                        {
                            //if the move is to a cell assigned to 0, highlight blue as a possible move
                            if (board[x + moveX][y + moveY] == PlayerID.NO_PLAYER)
                            {
                                cells[x + moveX][y + moveY].highlightCell(Colour.LIGHTBLUE);
                            }
                            //else it is to a cell containing a piece of the oppositions, highlight red
                            else
                            {
                                cells[x + moveX][y + moveY].highlightCell(Colour.RED);
                            }
                        }
                        //Else if the cell is assigned to the player, and the class is not the same as the selected piece
                        //and if there is no merged match
                        else
                        {
                            Piece destinationPiece = chessBoard.getPieces()[x + moveX][y + moveY];
                            if (!destinationPiece.isMerged() && !destinationPiece.getClass().equals(selectedPiece.getClass()) && range(0, selectedPiece.getMergedWith().size()).noneMatch(i -> selectedPiece.getMergedWith().get(i).getClass().equals(destinationPiece.getClass())))
                            {
                                //highlight the cell orange (as a possible merge)
                                cells[x + moveX][y + moveY].highlightCell(Colour.ORANGE);
                            }
                        }
                    }
                }
                //Exception ignored as it is a move that is off the board
                catch (ArrayIndexOutOfBoundsException ignored)
                {
                }
            }
        }
        //Highlight the selected piece's cell green
        cells[x][y].highlightCell(Colour.GREEN);
    }

    //Once a destination cell is selected, this method directs movement of the piece
    public void movePiece(final int x, final int y)
    {
        //Local Piece variable to reduce getSelected piece method calls
        Piece selectedPiece = chessBoard.getSelectedPiece();
        PlayerID board = chessBoard.getBoard()[x][y];

        //If the destination selection is the same cell as the selected piece
        if (x == selectedPiece.getxPosition() && y == selectedPiece.getyPosition())
        {
            //unhighlight all cells and set selected piece details to null
            unhighlightCells();
            selectedPiece.setUnMerge(false);
            chessBoard.setSelectedPiece(null);
        }
        //else if the cell is highlighted
        else if (cells[x][y].isCellHighlighted())
        {
            //Check to see if the move is going to put the piece in a vulnerable position
            if (!checkDangerousMove(x, y) || PopUpView.dangerMovePrompt().equals("OK"))
            {
                //if the player is unMerging pieces
                if (selectedPiece.isUnMerge())
                {
                    //call the unMergePiece method
                    unMergePiece(selectedPiece, x, y);
                }
                //else if the cell being moved to contains one of the current players pieces
                else if (board == chessBoard.getPlayersTurn())
                {
                    //merge the pieces
                    mergePiece(selectedPiece, x, y);
                }
                //If the cell being moved to contains an opponents piece
                else if (board != PlayerID.NO_PLAYER)
                {
                    //update the score ( pass x,y coordinates to check if the captured piece is merged
                    // and multiply score by merged list size.
                    updateScore(x, y);

                    //Call capture piece method
                    capturePiece(selectedPiece, x, y);
                }
                else
                {
                    //else call the updateBoard method to move the piece into a vacant
                    updateBoard(selectedPiece, x, y);
                }

                //unhighlight all cells
                unhighlightCells();

                //unselect the piece
                chessBoard.setSelectedPiece(null);

                //update game details
                updateMoveHistory();
                updateTurn();
                checkGameStatus();
            }
        }
    }

    public boolean checkDangerousMove(int x, int y)
    {
        //Declare and initialise boolean dangerousMove, and loop iterator i
        boolean dangerousMove = false;
        int i = 0;

        //loop over the pieces 2 dimensional array and check the pieces moves to see if the selected destination
        //will put the players piece in a vulnerable position
        while (i < chessBoard.getBoardSize() && !dangerousMove)
        {
            int j = 0;
            while (j < chessBoard.getBoardSize() && !dangerousMove)
            {
                Piece piece = chessBoard.getPieces()[i][j];
                if (piece != null && piece.getColour() != chessBoard.getPlayersTurn())
                {
                    //for each move that the selected piece has in its moves HashSet
                    for (Move move : piece.getMoves())
                    {
                        int moveX = move.getX();
                        int moveY = move.getY();
                        int pieceX = piece.getxPosition();
                        int pieceY = piece.getyPosition();

                        //get the destination x & y co-ordinates
                        int destinationX = moveX + pieceX;
                        int destinationY = moveY + pieceY;

                        try
                        {
                            //Check to make sure there is not a piece in the way of a potential capture piece
                            if (!(piece instanceof Knight) && (Math.abs(moveX) == 2 || Math.abs(moveY) == 2))
                            {
                                //Initialise int values for the adjacent square
                                int adjacentX = (destinationX + pieceX) / 2;
                                int adjacentY = (destinationY + pieceY) / 2;

                                //Check that the blocking piece isn't the piece that is being moved
                                if (adjacentX != chessBoard.getSelectedPiece().getxPosition() && adjacentY != chessBoard.getSelectedPiece().getyPosition() && chessBoard.getBoard()[adjacentX][adjacentY] != PlayerID.NO_PLAYER)
                                {
                                    continue;
                                }
                            }
                        }
                        //Exception ignored as it is a move that is off the board
                        catch (ArrayIndexOutOfBoundsException ignored)
                        {
                        }
                        //If the destination is dangerous, prompt player
                        if (destinationX == x && destinationY == y)
                        {
                            dangerousMove = true;
                            break;
                        }
                    }
                }
                j++;
            }
            i++;
        }
        return dangerousMove;
    }

    //Merge pieces (selected piece and destination piece)
    private void mergePiece(Piece selectedPiece, int x, int y)
    {
        Piece mergePiece = chessBoard.getPieces()[x][y];
        //Add the merged pieces moves into the selectedPiece
        selectedPiece.addMoves(mergePiece.getMoves());

        //Add the merged piece to the selected pieces merged with list
        selectedPiece.getMergedWith().add(mergePiece);

        //Set the status of the selected piece to merged
        selectedPiece.setMerged(true);

        //remove the merged piece from the view
        boardView.removePiece(mergePiece.getPieceID());

        //Update the board details
        updateBoard(selectedPiece, x, y);
    }

    //UnMerge pieces from the selectedPiece
    private void unMergePiece(Piece selectedPiece, int x, int y)
    {
        Piece unMergePiece = selectedPiece.getUnMergePiece();
        //Get the unmerged piece from the selected piece and update its position to the destination co-ordinates
        unMergePiece.setxPosition(x);
        unMergePiece.setyPosition(y);

        //Add the piece to the board array
        chessBoard.getBoard()[x][y] = unMergePiece.getColour();

        //Add the piece to the pieces array
        chessBoard.getPieces()[x][y] = unMergePiece;

        //Remove the merged moves from the selected piece
        selectedPiece.removeMoves(unMergePiece.getMoves());

        //Add the piece back to the view
        boardView.addPiece(unMergePiece, x, y);

        //reset unMerge
        selectedPiece.setUnMerge(false);

        //Remove the unMerged piece from the selected pieces merged with list
        selectedPiece.getMergedWith().remove(unMergePiece);

        //If the mergedWith list is empty, set the isMerged status to false
        if (selectedPiece.getMergedWith().size() == 0)
        {
            selectedPiece.setMerged(false);
        }
    }

    //capture piece method is called from movePiece in Piece class to update the board and view
    private void capturePiece(Piece selectedPiece, int x, int y)
    {
        //remove the piece from the view
        boardView.removePiece(chessBoard.getPieces()[x][y].getPieceID());

        //update the chessboard
        updateBoard(selectedPiece, x, y);
    }

    //Update players scores and update board view scores
    private void updateScore(int x, int y)
    {
        chessBoard.getPlayers()[chessBoard.getPlayersTurn().ordinal()].setScore(chessBoard.getPlayers()[chessBoard.getPlayersTurn().ordinal()].getScore() + (5 * (chessBoard.getPieces()[x][y].getMergedWith().size() + 1)));
        boardView.updateScore(chessBoard.getPlayersTurn().ordinal());
    }

    //updateBoard method is to update the chessboard and the view
    private void updateBoard(Piece selectedPiece, int x, int y)
    {
        //Clear old positions for the board and piece arrays
        chessBoard.updateBoard(selectedPiece.getxPosition(), selectedPiece.getyPosition(), PlayerID.NO_PLAYER);
        chessBoard.setPiece(selectedPiece.getxPosition(), selectedPiece.getyPosition(), null);

        //Update the position reference of the selected piece
        selectedPiece.setxPosition(x);
        selectedPiece.setyPosition(y);

        //Update new position for the board and piece arrays
        chessBoard.updateBoard(x, y, chessBoard.getPlayersTurn());
        chessBoard.setPiece(x, y, selectedPiece);

        //Update the board view
        boardView.setGrid(selectedPiece.getPieceID(), x, y);
    }

    //unhighlight all cells
    private void unhighlightCells()
    {
        for (int y = 0; y < chessBoard.getBoardSize(); y++)
        {
            for (int x = 0; x < chessBoard.getBoardSize(); x++)
            {
                if (cells[x][y].isCellHighlighted())
                {
                    cells[x][y].unhighlightCell();
                }
            }
        }
    }

    private void updateMoveHistory()
    {
        //capture players scores for move history
        int[] scores = new int[]{chessBoard.getPlayers()[0].getScore(), chessBoard.getPlayers()[1].getScore()};

        chessBoard.getMoveHistory().add(new MoveHistory(chessBoard.getBoard(), chessBoard.getPieces(), scores));

        //Update moves played and moves remaining
        chessBoard.setMovesPlayed(chessBoard.getMovesPlayed() + 1);
        chessBoard.setMovesRemaining(chessBoard.getMovesRemaining() - 1);

        //Update board view
        boardView.updateMoves(chessBoard.getMovesPlayed(), chessBoard.getMovesRemaining());
    }

    //update players turn
    private void updateTurn()
    {
        if (chessBoard.getPlayersTurn() == PlayerID.WHITE)
        {
            chessBoard.setPlayersTurn(PlayerID.BLACK);
        }
        else
        {
            chessBoard.setPlayersTurn(PlayerID.WHITE);
        }

        boardView.updateTurn(chessBoard.getPlayersTurn());
    }

    //Undo the last two moves
    public void undoLast2Moves()
    {
        //Remove all pieces from the view
        boardView.removeAllPieces(chessBoard.getPieces());
        MoveHistory previousMove = chessBoard.getMoveHistory().get(chessBoard.getMoveHistory().size() - 3);

        //Update the board and pieces with the previous configuration
        chessBoard.setBoard(previousMove.getBoard());
        chessBoard.setPieces(previousMove.getPieces());

        //Update the players scores
        Player[] players = chessBoard.getPlayers();

        players[0].setScore(previousMove.getScores()[0]);
        players[1].setScore(previousMove.getScores()[1]);

        //Update the moves played and remaining to reflect the undone moves + another move played
        chessBoard.setMovesPlayed(chessBoard.getMovesPlayed() - 1);
        chessBoard.setMovesRemaining(chessBoard.getMovesRemaining() + 1);

        //Add updated pieces to the view as well as updating scores and moves information
        boardView.addAllPieces(chessBoard.getPieces());
        range(0, players.length).forEach(i -> boardView.updateScore(i));
        boardView.updateMoves(chessBoard.getMovesPlayed(), chessBoard.getMovesRemaining());

        //Update players turn
        updateTurn();

        //Remove last 2 moves from move history
        chessBoard.getMoveHistory().remove(chessBoard.getMoveHistory().size() - 1);
        chessBoard.getMoveHistory().remove(chessBoard.getMoveHistory().size() - 2);
    }

    //Check game status after each move to see if a winner has prevailed
    private void checkGameStatus()
    {
        Player[] players = chessBoard.getPlayers();
        //If the moves remaining has run out, compare scores and notify winner or notify players it was a draw.
        //Then prompt with rematch/ext options.
        if (chessBoard.getMovesRemaining() == 0)
        {
            if (players[0].getScore() > players[1].getScore())
            {
                PopUpView.message(players[0].getUsername() + " is the winner!", players[0].getUsername() + " wins!");
            }
            else if (players[0].getScore() < players[1].getScore())
            {
                PopUpView.message(players[1].getUsername() + " is the winner!", players[1].getUsername() + " wins!");
            }
            else
            {
                PopUpView.message("The game is a draw!", "Draw!");
            }
            rematch(players);
        }
        else
        {
            //Check each players scores. If a player has a score of 30, they have captured all of their opponents pieces
            //and are pronounced the winner
            //Prompt winner with rematch / exit options
            for (Player player : players)
            {
                if (player.getScore() == 30)
                {
                    PopUpView.message(player.getUsername() + " is the winner!", player.getUsername() + " wins!");
                    rematch(players);
                }
            }
        }
    }

    //Rematch method determines what action to take after the game is completed
    private void rematch(Player[] players)
    {
        //Prompt player with options upon game completion (Rematch, logout or exit)
        String selection = PopUpView.rematchPrompt();

        if (selection.equals("Rematch"))
        {
            restartGame(players);
        }
        else if (selection.equals("Logout"))
        {
            chessBoard = new ChessBoard();
            newLogin();
        }
        else
        {
            boardView.exit();
        }
    }

    //Restart game called from view at anytime during game play
    public void restartGame(Player[] players)
    {
        //Declare and initialise a temporary players array
        Player[] tempPlayers = new Player[2];
        tempPlayers[0] = new Player(players[1].getUsername(), players[1].getMaxMoves(), 0);
        tempPlayers[1] = new Player(players[0].getUsername(), players[0].getMaxMoves(), 0);

        //Reinitialise the chessboard
        chessBoard = new ChessBoard();

        //Set the players on the new chessboard with the temp player details
        chessBoard.setPlayers(tempPlayers);

        //Restart game
        CheckMate.restartGame();
    }
}