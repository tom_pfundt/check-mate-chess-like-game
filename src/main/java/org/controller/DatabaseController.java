package org.controller;

import org.model.Player;
import org.view.PopUpView;
import org.view.RegisterLoginView;

import java.sql.*;

import static org.CheckMate.getGameController;

public class DatabaseController
{
    //Declare Database connection and login view variables
    private static Connection connection;
    private RegisterLoginView loginView;

    //Method init initialises a connection with the database and returns the connection if successful
    public static void init() throws SQLException
    {
            connection = DriverManager.getConnection("jdbc:mariadb://localhost/SEF", "root", "");
            createTable();
    }

    //When connecting to the database, check to make sure the table exists. If it doesn't, the program will create it
    static void createTable() throws SQLException
    {
        Statement createTable = connection.createStatement();
        createTable.executeUpdate("CREATE TABLE IF NOT EXISTS sef.checkmate (username varchar(20), password varchar(40), PRIMARY KEY (username))");
        createTable.close();
    }

    //Build login view populates the login view
    public void buildLoginView()
    {
        this.loginView = new RegisterLoginView();
    }

    public RegisterLoginView getLoginView()
    {
        return loginView;
    }

    //register method used for processing registration requests
    public static void register(String username, String password, String moves)
    {
        //check the validity of the values entered by the player. If the inputs are valid, continue
        boolean validInput = checkStringValidity(username, password, moves);

        if (validInput)
        {


            try
            {
                //If there is no current connection, initialise one
                if (connection == null)
                {
                    init();
                }

                //Build SQL query string to check if a username already exists
                PreparedStatement prepStmt = connection.prepareStatement("Select * FROM sef.checkmate WHERE username like ?;");

                //Use binding to protect from SQL injection
                prepStmt.setString(1, username);

                //Execute SQL Query
                ResultSet resultSet = prepStmt.executeQuery();

                //If the username does exist, throw a registration exception
                if (resultSet.next())
                {
                    PopUpView.message("Username already exists", "Try again");
                }
                else
                {
                    //else, insert the registered details into the table
                    prepStmt = connection.prepareStatement("INSERT into sef.checkmate Values(?, SHA(?));");

                    //Use binding to protect from SQL injection
                    prepStmt.setString(1, username);
                    prepStmt.setString(2, password);

                    prepStmt.executeQuery();

                    //Prepare SQL Query to confirm details were entered correctly
                    prepStmt = connection.prepareStatement("Select * FROM sef.checkmate WHERE username like ? and password like SHA(?);");

                    //Use binding to protect from SQL injection
                    prepStmt.setString(1, username);
                    prepStmt.setString(2, password);

                    //Execute query
                    resultSet = prepStmt.executeQuery();

                    //If the details have not been entered, throw a registration exception
                    if (!resultSet.next())
                    {
                        PopUpView.message("Registration Failed", "Failed");
                    }
                    else
                    {
                        //If registration was successful, notify player with a pop up message and create the player
                        PopUpView.message("Registration Successful. You have been automatically logged in.", "Success");

                        createPlayer(username, moves);
                    }
                }
            }
            catch (SQLException ignored)
            {
            }
        }
    }

    public static void login(String username, String password, String moves)
    {
        //check the validity of the values entered by the player. If the inputs are valid, continue
        boolean validInput = checkStringValidity(username, password, moves);

        if (validInput)
        {
            Player[] players = getGameController().getChessBoard().getPlayers();

            //If player[0] is null or the username input does not match that of player[0]
            if (players[0] == null || !username.equals(players[0].getUsername()))
            {
                try
                {
                    //Get database connection
                    if (connection == null)
                    {
                        init();
                    }

                    //Prepare SQL Query to confirm details were entered correctly
                    PreparedStatement prepStmt = connection.prepareStatement("Select * FROM sef.checkmate WHERE username like ? and password like SHA(?);");

                    //Use binding to protect from SQL injection
                    prepStmt.setString(1, username);
                    prepStmt.setString(2, password);

                    //Execute query
                    ResultSet resultSet = prepStmt.executeQuery();

                    //If there is no match, throw incorrect login exception
                    if (!resultSet.next())
                    {
                        PopUpView.message("The Username or Password are incorrect, please re-enter your Username and Password.", "Failed");
                    }
                    else
                    {
                        //else notify player that login was successful, and create player
                        PopUpView.message("Login Successful.", "Success");

                        createPlayer(username, moves);
                    }
                }
                catch (SQLException ignored)
                {
                }
            }
            else
            {
                //If the username input matches that of player 1, notify player they must login as someone else
                PopUpView.message(username + " is already logged in as Player 1. Please log in as someone else.", "Already Logged In");
            }
        }
    }

    private static boolean checkStringValidity(String username, String password, String moves)
    {

        boolean validInput = false;
        if (username.isEmpty())
        {
            PopUpView.message("The username must contain characters, please re-enter your username", "Empty Username");
        }
        else if (password.isEmpty())
        {
            PopUpView.message("The password must contain characters, please re-enter your password", "Empty Password");
        }
        else if (moves.isEmpty())
        {
            PopUpView.message("The moves must contain numerical digits, please re-enter your moves", "Empty moves");
        }
        else
        {
            try
            {
                int tempMoves = Integer.parseInt(moves);

                if (tempMoves <= 0)
                {
                    PopUpView.message("The moves must be greater than 0, please re-enter your moves", "Zero moves");
                }
                else
                {
                    validInput = true;
                }
            }
            catch (NumberFormatException e)
            {
                PopUpView.message("The moves must contain numerical digits, please re-enter your moves", "Wrong Format");
            }
        }
        return validInput;
    }

    private static void createPlayer(String username, String moves)
    {
        Player[] players = getGameController().getChessBoard().getPlayers();
        //If player[0] is null
        if (players[0] == null)
        {
            //load player details into player[0]
            players[0] = new Player(username, Integer.parseInt(moves), 0);
        }
        else
        {
            //else load player details into player[1]
            players[1] = new Player(username, Integer.parseInt(moves), 0);
        }
    }
}