package org;

import org.controller.GameController;
import org.controller.DatabaseController;
import org.model.ChessBoard;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.SQLException;

public class CheckMate extends Application
{

    //Private fields for building the stage, the scene and a chessboard controller instance
    private static Stage stage;
    private static Scene initialScene;
    public static GameController gameController;


    // overridden Application init method
    @Override
    public void init()
    {
        gameController = new GameController();

        initialScene = loginScene();
    }
    // overridden Application start method
    @Override
    public void start(Stage primaryStage)
    {
        stage = primaryStage;
        //fields for this application
        // set the title and scene, fix the stage size, and show the stage
        stage.setTitle("Check Mate");
        stage.setScene(initialScene);
        stage.show();

        checkConnection();
    }

    public static Scene loginScene()
    {
        // initialize the stackPane and build the login screen grid

        DatabaseController dbController = new DatabaseController();
        dbController.buildLoginView();

        return new Scene(dbController.getLoginView().getBuiltScene(), 300, 300);
    }

    public static Scene chessBoardScene()
    {
        // initialize the stackPane and build the chessboard screen grid
        ChessBoard chessBoard = gameController.getChessBoard();
        chessBoard.setMovesRemaining((chessBoard.getPlayers()[0].getMaxMoves() + chessBoard.getPlayers()[1].getMaxMoves()) / 2);
        gameController.buildBoardView();

        return new Scene(gameController.getBoardView().getBuiltScene(), 400, 450);
    }

    //Method to test database connection
    private static void checkConnection()
    {
        try
        {
            DatabaseController.init();
        }
        catch (SQLException e)
        {
            gameController.getChessBoard().initialiseDefaultPlayers();

            //Prompt players notifying that the database connection has failed and players are set to default
            gameController.loginFailure();

            //close stage, build chessboard and re-open stage
            stage.close();
            stage.setScene(chessBoardScene());
            stage.show();
        }
    }

    public static void restartGame()
    {
        //close stage, build chessboard and re-open stage
        stage.close();
        stage.setScene(chessBoardScene());
        stage.show();
    }

    public static void newLogin()
    {
        //close stage, build chessboard and re-open stage
        stage.close();
        stage.setScene(loginScene());
        stage.show();

        checkConnection();
    }

    //Getter for chessboard controller
    public static GameController getGameController()
    {
        return gameController;
    }

    //main method;
    public static void main(String[] args)
    {
        launch(args);
    }
}