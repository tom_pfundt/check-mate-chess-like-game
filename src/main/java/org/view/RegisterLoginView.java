package org.view;

import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.CheckMate;
import com.sun.javafx.robot.impl.FXRobotHelper;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import static org.controller.DatabaseController.*;

public class RegisterLoginView extends Pane
{
    private final TextField username;
    private final PasswordField password;
    private final TextField maxMoves;
    private final VBox scene;
    private final GridPane grid;

    public RegisterLoginView()
    {
        username = new TextField();
        username.setId("username");
        password = new PasswordField();
        password.setId("password");
        maxMoves = new TextField();
        maxMoves.setId("maxMoves");
        scene = new VBox();
        grid = new GridPane();

        renderMenu();

        renderLoginPage();
    }

    private void renderMenu()
    {
        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");

        fileMenu.setId("fileMenu");

        MenuItem exit = new MenuItem("Exit");
        exit.setId("exit");

        exit.setOnAction(event -> {
            Stage stage = FXRobotHelper.getStages().get(0);
            stage.close();
        });

        fileMenu.getItems().add(exit);

        menuBar.getMenus().addAll(fileMenu);

        scene.getChildren().add(menuBar);
    }

    private void renderLoginPage()
    {
        //Declare and initialise Labels
        Label title = new Label("Player 1 Login");
        title.setId("title");
        Label userText = new Label("Username:");
        Label passwordText = new Label("Password:");
        Label maxMovesText = new Label("Maximum Moves:");

        //Declare and initialise Buttons
        Button register = new Button("Register");
        register.setId("register");
        Button login = new Button("Login");
        login.setId("login");

        HBox buttons = new HBox();
        buttons.setSpacing(15.0);
        buttons.getChildren().addAll(register, login);

        GridPane innerGrid = new GridPane();
        innerGrid.add(buttons, 0, 0);
        innerGrid.setAlignment(Pos.CENTER);

        grid.setHgap(10);
        grid.setVgap(10);

        ColumnConstraints column0 = new ColumnConstraints();
        column0.setHalignment(HPos.RIGHT);
        grid.getColumnConstraints().add(column0);

        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.LEFT);
        grid.getColumnConstraints().add(column1);

        register.setOnAction(event -> {
            register(username.getText(), password.getText(), maxMoves.getText());
            updateView();
        });
        login.setOnAction(event -> {
            login(username.getText(), password.getText(), maxMoves.getText());
            updateView();
        });


        GridPane titleGrid = new GridPane();
        titleGrid.setAlignment(Pos.CENTER);
        titleGrid.add(title, 0, 0);

        grid.add(titleGrid, 0, 0, 2, 1);
        grid.add(userText, 0, 1);
        grid.add(username, 1, 1);
        grid.add(passwordText, 0, 2);
        grid.add(password, 1, 2);
        grid.add(maxMovesText, 0, 3);
        grid.add(maxMoves, 1, 3);
        grid.add(innerGrid, 0, 4, 2, 1);

        grid.setAlignment(Pos.CENTER);

        scene.getChildren().add(grid);
    }

    public VBox getBuiltScene()
    {
        return scene;
    }

    private void updateView()
    {
        Scene scene = FXRobotHelper.getStages().get(0).getScene();
        if (CheckMate.getGameController().getChessBoard().getPlayers()[0] != null)
        {
            if (CheckMate.getGameController().getChessBoard().getPlayers()[1] == null)
            {
                Label title = (Label) scene.lookup("#title");
                title.setText("Player 2 Login");
                setFieldsEmpty(scene);
            }
            else
            {
                Stage stage = FXRobotHelper.getStages().get(0);
                stage.close();
                stage.setScene(CheckMate.chessBoardScene());
                stage.show();
            }
        }
        else
        {
            setFieldsEmpty(scene);
        }
    }

    private void setFieldsEmpty(Scene scene)
    {
        TextField username = (TextField) scene.lookup("#username");
        username.setText("");
        PasswordField password = (PasswordField) scene.lookup("#password");
        password.setText("");
        TextField maxMoves = (TextField) scene.lookup("#maxMoves");
        maxMoves.setText("");
    }
}