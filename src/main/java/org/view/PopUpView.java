package org.view;

import org.model.Piece;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class PopUpView
{

    public static String prompt(Piece selectedPiece)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Split Piece");
        alert.setHeaderText("Split Piece Selection");
        alert.setContentText("If you wish to split your pieces, select which piece you wish to recover, else press cancel to take your move.");

        ArrayList<ButtonType> buttons = selectedPiece.getMergedWith().stream().map(piece -> new ButtonType(piece.getClass().getSimpleName())).collect(Collectors.toCollection(ArrayList::new));

        buttons.add(new ButtonType("Cancel"));

        alert.getButtonTypes().setAll(buttons);

        return getResult(alert);
    }
    public static String rematchPrompt()
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Game Over");
        alert.setHeaderText("Would you like to play again?");

        ButtonType rematch = new ButtonType("Rematch");
        ButtonType logout = new ButtonType("Logout");
        ButtonType quit = new ButtonType("Quit");

        alert.getButtonTypes().setAll(rematch, logout, quit);

        return getResult(alert);
    }

    public static void message(String message, String title)
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setContentText(message);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.showAndWait();
    }

    public static String dangerMovePrompt()
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Dangerous Move");
        alert.setHeaderText("Do you wish to make this dangerous move?");
        alert.setContentText("The move you are attempting to make will put your piece in a vulnerable position. Are you sure you wish to make this move?");

        return getResult(alert);
    }
    private static String getResult(Alert alert)
    {
        Optional<ButtonType> result = alert.showAndWait();

        AtomicReference<String> text = new AtomicReference<>("");
        result.ifPresent(name -> text.set(name.getText()));

        return text.get();
    }
}
