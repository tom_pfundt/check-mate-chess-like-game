package org.view;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import org.model.Colour;

public class BoardCellView extends Group
{
    //Declare rectangle for each cell on the board
    private final Rectangle cell;
    private Color strokeColour;
    private String previousStrokeColour;

    //Declare variable to track if the cell is highlighted
    private boolean isHighlighted = false;

    // constructor for the class
    BoardCellView(int i, String id)
    {
        // Create a new Rectangle and set dimensions as well as cell ID
        cell = new Rectangle();
        cell.setWidth(60.0);
        cell.setHeight(60.0);
        cell.setId(id);
        //If the colour index is 0 fill the cell saddlebrown, else fill it sandy brown
        if (i == 0)
        {
            cell.setFill(Color.SADDLEBROWN);
        }
        else
        {
            cell.setFill(Color.SANDYBROWN);
        }

        //Add the cell to the stage Group
        getChildren().add(cell);
    }

    //This method is used to highlight the border of the cell to highlight potential moves for the player
    public void highlightCell(Colour colour)
    {
        //Set the stroke type to be inside the cell and a width of 4px
        cell.setStrokeType(StrokeType.INSIDE);
        cell.setStrokeWidth(4);

        //Set the cell highlighted state to true
        isHighlighted = true;

        //Depending on the colour passed from the showPossibleMoves method in the game controller, highlight
        //the cell border accordingly:
        //green - this is the selected piece cell
        //light blue - this is a vacant cell the selected piece can move to
        //Red - this cell contains an opponents piece that can be captured
        //Orange - this cell contains one of the players pieces that can be merged with the selected
        //piece. Also orange indicates cells where a piece that is being split can be placed
        //CRIMSON when the cell is hovered over and is a dangerous move

        if (cell.getStroke() != null)
        {
            previousStrokeColour = strokeColour.toString();
        }
        switch (colour)
        {
            case GREEN:
                strokeColour = Color.GREEN;
                break;
            case LIGHTBLUE:
                strokeColour = Color.LIGHTBLUE;
                break;
            case RED:
                strokeColour = Color.RED;
                break;
            case ORANGE:
                strokeColour = Color.ORANGE;
                break;
            case CRIMSON:
                strokeColour = Color.CRIMSON;

                break;
        }
        cell.setStroke(strokeColour);

    }

    //Unhighlight cell method is used to remove the border colour from the cell once the player has moved or de-selected
    //their piece
    public void unhighlightCell()
    {
        cell.setStroke(null);
        isHighlighted = false;
    }

    //Getter for highlight status
    public boolean isCellHighlighted()
    {
        return (isHighlighted);
    }

    Color getStrokeColour()
    {
        return strokeColour;
    }

    String getPreviousStrokeColour()
    {
        return previousStrokeColour;
    }
}
