package org.view;

import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import org.CheckMate;
import org.controller.GameController;
import org.model.ChessBoard;
import org.model.Colour;
import org.model.Piece;
import org.model.Player;
import com.sun.javafx.robot.impl.FXRobotHelper;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.model.PlayerID;

import static com.sun.javafx.robot.impl.FXRobotHelper.getStages;
import static javafx.scene.layout.GridPane.getColumnIndex;
import static javafx.scene.layout.GridPane.getRowIndex;

public class ChessBoardView extends Pane
{
    private final VBox scene;
    private final GridPane grid;
    private final GameController gameController;

    public ChessBoardView(GameController gameController)
    {
        scene = new VBox();
        grid = new GridPane();
        this.gameController = gameController;

        //Build and render the border pane
        renderMenu(this.gameController.getChessBoard());

        //Render the board cells and the piece images
        renderBoard();

        //Build and render the control pane
        renderInfoPane(this.gameController.getChessBoard());
    }

    private void renderMenu(ChessBoard chessBoard)
    {
        Player[] players = chessBoard.getPlayers();

        MenuBar menuBar = new MenuBar();
        Menu fileMenu = new Menu("File");
        Menu gameMenu = new Menu("Game Play");

        fileMenu.setId("fileMenu");
        gameMenu.setId("gameMenu");

        MenuItem p1Undo = new MenuItem(chessBoard.getPlayers()[0].getUsername() + " Undo Move");
        p1Undo.setId("p1Undo");
        p1Undo.setOnAction(event ->
                           {
                               if (chessBoard.getMoveHistory().size() > 2 && chessBoard.getPlayersTurn() == PlayerID.WHITE && chessBoard.getSelectedPiece() == null)
                               {
                                   CheckMate.getGameController().undoLast2Moves();
                                   MenuItem menuItem = (MenuItem) event.getSource();
                                   menuBar.getMenus().get(1).getItems().remove(menuItem);
                               }
                           });

        MenuItem p2Undo = new MenuItem(players[1].getUsername() + " Undo Move");
        p2Undo.setId("p2Undo");
        p2Undo.setOnAction(event ->
                           {
                               if (chessBoard.getMoveHistory().size() > 2 && chessBoard.getPlayersTurn() == PlayerID.BLACK && chessBoard.getSelectedPiece() == null)
                               {
                                   CheckMate.getGameController().undoLast2Moves();
                                   MenuItem menuItem = (MenuItem) event.getSource();
                                   menuBar.getMenus().get(1).getItems().remove(menuItem);
                               }
                           });

        MenuItem exit = new MenuItem("Exit");
        exit.setId("exit");
        exit.setOnAction(event -> exit());
        MenuItem restart = new MenuItem("Restart");
        restart.setId("restart");
        restart.setOnAction(event -> this.gameController.restartGame(players));

        fileMenu.getItems().addAll(restart, exit);
        gameMenu.getItems().addAll(p1Undo, p2Undo);

        menuBar.getMenus().addAll(fileMenu, gameMenu);

        scene.getChildren().add(menuBar);
    }

    private void renderBoard()
    {
        renderRowColumn();

        renderPieceImages();
    }

    private void renderRowColumn()
    {
        // nested while loops to populate arrays to default values and add blank cells to the board
        for (int i = 0; i < gameController.getChessBoard().getBoardSize(); i++)
        {
            boolean isDark = false;
            if ((i % 2) == 0)
            {
                isDark = true;
            }
            renderColumn(i, isDark);
        }
    }

    private void renderColumn(int i, boolean isDark)
    {
        BoardCellView[][] cells = this.gameController.getCells();
        for (int j = 0; j < gameController.getChessBoard().getBoardSize(); j++)
        {
            StringBuilder id = new StringBuilder();
            id.append(i).append(j);
            if (isDark)
            {
                cells[i][j] = new BoardCellView(0, id.toString()); //SADDLEBROWN
                isDark = false;
            }
            else
            {
                cells[i][j] = new BoardCellView(1, id.toString()); //SANDYBROWN
                isDark = true;
            }
            cells[i][j].setOnMouseClicked(this::getCellDetails);
            cells[i][j].setOnMouseEntered(this::checkDangerousMove);
            cells[i][j].setOnMouseExited(this::revertHighlightColour);
            grid.add(cells[i][j], i, j);
        }
    }

    private void renderPieceImages()
    {
        this.grid.add(renderImageView("wR1", 0, 0), 0, 0);
        this.grid.add(renderImageView("wK1", 1, 0), 1, 0);
        this.grid.add(renderImageView("wB1", 2, 0), 2, 0);
        this.grid.add(renderImageView("wB2", 3, 0), 3, 0);
        this.grid.add(renderImageView("wK2", 4, 0), 4, 0);
        this.grid.add(renderImageView("wR2", 5, 0), 5, 0);
        this.grid.add(renderImageView("bR1", 0, 5), 0, 5);
        this.grid.add(renderImageView("bK1", 1, 5), 1, 5);
        this.grid.add(renderImageView("bB1", 2, 5), 2, 5);
        this.grid.add(renderImageView("bB2", 3, 5), 3, 5);
        this.grid.add(renderImageView("bK2", 4, 5), 4, 5);
        this.grid.add(renderImageView("bR2", 5, 5), 5, 5);
    }

    private ImageView renderImageView(String id, int x, int y)
    {
        Image pieceImage = new Image(this.gameController.getChessBoard().getPieces()[x][y].getImageDir());
        ImageView imageView = new ImageView(pieceImage);
        imageView.setId(id);
        imageView.setOnMouseClicked(this::getCellDetails);
        imageView.setOnMouseEntered(this::checkDangerousMove);
        imageView.setOnMouseExited(this::revertHighlightColour);
        return imageView;
    }

    private void renderInfoPane(ChessBoard chessBoard)
    {
        Player[] players = chessBoard.getPlayers();

        Label playersTurn = new Label((players[0].getUsername() + "'s Turn").toUpperCase());
        playersTurn.setId("playersTurn");
        playersTurn.setTextFill(Color.FORESTGREEN);

        Label p1Score = new Label(players[0].getUsername() + "'s score: 0");
        Label p2Score = new Label(players[1].getUsername() + "'s score: 0");
        p1Score.setId("p1Score");
        p2Score.setId("p2Score");

        Label movesPlayed = new Label("Moves Played: " + chessBoard.getMovesPlayed());
        Label movesRemaining = new Label("Moves Remaining: " + chessBoard.getMovesRemaining());
        movesPlayed.setId("movesPlayed");
        movesRemaining.setId("movesRemaining");

        int boardSize = chessBoard.getBoardSize();

        grid.add(playersTurn, 0, boardSize + 1, boardSize, 1);
        grid.add(p1Score, 0, boardSize + 2, boardSize / 2, 1);
        grid.add(p2Score, 0, boardSize + 3, boardSize / 2, 1);
        grid.add(movesPlayed, boardSize / 2, boardSize + 2, boardSize / 2, 1);
        grid.add(movesRemaining, boardSize / 2, chessBoard.getBoardSize() + 3, boardSize / 2, 1);

        this.grid.setAlignment(Pos.CENTER);

        scene.getChildren().add(grid);
    }

    public VBox getBuiltScene()
    {
        return scene;
    }

    private void getCellDetails(MouseEvent event)
    {
        Node source = (Node) event.getSource();
        if (this.gameController.getChessBoard().getSelectedPiece() == null)
        {
            if (source instanceof ImageView)
            {
                this.gameController.selectPiece(getColumnIndex(source), getRowIndex(source));
            }
        }
        else
        {
            this.gameController.movePiece(getColumnIndex(source), getRowIndex(source));
        }
    }

    private void checkDangerousMove(MouseEvent event)
    {
        Node source = (Node) event.getSource();
        int x = getColumnIndex(source);
        int y = getRowIndex(source);

        if ((gameController.getCells()[x][y].isCellHighlighted() && gameController.checkDangerousMove(x, y)))
        {
            gameController.getCells()[x][y].highlightCell(Colour.CRIMSON);
        }
    }

    private void revertHighlightColour(MouseEvent event)
    {
        Node source = (Node) event.getSource();
        int x = getColumnIndex(source);
        int y = getRowIndex(source);

        if (gameController.getCells()[x][y].isCellHighlighted() && gameController.getCells()[x][y].getStrokeColour().equals(Color.CRIMSON))
        {
            Colour colour = null;
            switch (gameController.getCells()[x][y].getPreviousStrokeColour())
            {
                case "0x008000ff"://GREEN
                    colour = Colour.GREEN;
                    break;
                case "0xadd8e6ff"://LIGHTBLUE
                    colour = Colour.LIGHTBLUE;
                    break;
                case "0xff0000ff"://RED
                    colour = Colour.RED;
                    break;
                case "0xFFA500ff":
                    colour = Colour.ORANGE;
                    break;
            }

            gameController.getCells()[x][y].highlightCell(colour);
        }
    }

    public void setGrid(String id, int x, int y)
    {
        ImageView image = (ImageView) FXRobotHelper.getStages().get(0).getScene().lookup("#" + id);
        this.grid.getChildren().remove(image);
        image.setId(id);
        this.grid.add(image, x, y);
    }

    public void removeAllPieces(Piece[][] piecesArr)
    {
        for (Piece[] pieces : piecesArr)
        {
            for (Piece piece : pieces)
            {
                try
                {
                    removePiece(piece.getPieceID());
                }
                catch (NullPointerException ignored)
                {
                }
            }
        }
    }

    public void removePiece(String id)
    {
        ImageView image = (ImageView) FXRobotHelper.getStages().get(0).getScene().lookup("#" + id);
        this.grid.getChildren().remove(image);
    }

    public void addAllPieces(Piece[][] pieces)
    {
        for (int i = 0; i < pieces.length; i++)
        {
            for (int j = 0; j < pieces[i].length; j++)
            {
                if (pieces[i][j] != null)
                {
                    addPiece(pieces[i][j], i, j);
                }
            }
        }
    }

    public void addPiece(Piece piece, int x, int y)
    {
        this.grid.add(renderImageView(piece.getPieceID(), x, y), x, y);
    }

    public void updateScore(int playerIndex)
    {
        Label score = (Label) FXRobotHelper.getStages().get(0).getScene().lookup("#p" + (playerIndex + 1) + "Score");
        score.setText(this.gameController.getChessBoard().getPlayers()[playerIndex].getUsername() + "'s score: " + this.gameController.getChessBoard().getPlayers()[playerIndex].getScore());
    }

    public void updateMoves(int movesPlayed, int movesRemaining)
    {
        Scene scene = FXRobotHelper.getStages().get(0).getScene();
        Label movesPlayedLabel = (Label) scene.lookup("#movesPlayed");
        movesPlayedLabel.setText("Moves Played: " + movesPlayed);
        Label movesRemainingLabel = (Label) scene.lookup("#movesRemaining");
        movesRemainingLabel.setText("Moves Remaining: " + movesRemaining);
    }

    public void updateTurn(PlayerID playersTurn)
    {
        Label turn = (Label) FXRobotHelper.getStages().get(0).getScene().lookup("#playersTurn");

        if (playersTurn == PlayerID.WHITE)
        {
            turn.setTextFill(Color.FORESTGREEN);
        }
        else if (playersTurn == PlayerID.BLACK)
        {
            turn.setTextFill(Color.SADDLEBROWN);
        }

        turn.setText((this.gameController.getChessBoard().getPlayers()[playersTurn.ordinal()].getUsername() + "'s Turn").toUpperCase());
    }

    public void exit()
    {
        Stage stage = getStages().get(0);
        stage.close();
    }
}

