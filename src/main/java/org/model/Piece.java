package org.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

abstract public class Piece
{
    //Declare variables to capture the x and y positions of the piece.
    private int xPosition;
    private int yPosition;

    //Declare variable for which colour the piece is - 1 for white, 2 for black
    private final PlayerID colour;

    //define merged status boolean and mergedWith Piece list to track merged pieces
    private boolean merged;
    private final List<Piece> mergedWith;

    //Define unMerge status and unMergePiece variables for use when splitting a merged piece
    private boolean unMerge;
    private Piece unMergePiece;

    //Define HashSet for storing a pieces available moves
    final HashSet<Move> moves;

    //Declare variables for PieceID and image directory - these are used for the view
    private final String pieceID;
    private String imageDir;

    //Class constructor
    Piece(PlayerID playerColour, int xPosition, int yPosition, String pieceID)
    {
        this.colour = playerColour;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.pieceID = pieceID;
        merged = false;
        mergedWith = new ArrayList<>();
        unMerge = false;
        unMergePiece = null;
        this.moves = new HashSet<>();

        //Depending on piece colour, initialise directory string for image
        imageDir = "file:src/main/resources/PieceImages/" + this.getClass().getSimpleName() + this.getColour() + ".png";
    }

    public String getImageDir()
    {
        return imageDir;
    }

    public HashSet<Move> getMoves()
    {
        return moves;
    }

    public PlayerID getColour()
    {
        return colour;
    }

    public int getxPosition()
    {
        return xPosition;
    }

    public int getyPosition()
    {
        return yPosition;
    }

    public String getPieceID()
    {
        return pieceID;
    }

    public boolean isMerged()
    {
        return merged;
    }

    public void setMerged(boolean merged)
    {
        this.merged = merged;
    }

    public List<Piece> getMergedWith()
    {
        return mergedWith;
    }

    public Piece getUnMergePiece()
    {
        return unMergePiece;
    }

    public void setUnMergePiece(Piece unMergePiece)
    {
        this.unMergePiece = unMergePiece;
    }

    public boolean isUnMerge()
    {
        return unMerge;
    }

    public void setUnMerge(boolean unMerge)
    {
        this.unMerge = unMerge;
    }

    public void setxPosition(int xPosition)
    {
        this.xPosition = xPosition;
    }

    public void setyPosition(int yPosition)
    {
        this.yPosition = yPosition;
    }

    //Add moves into the HashSet of selected piece when piece is merged
    public void addMoves(HashSet<Move> moves)
    {
        this.moves.addAll(moves);
    }

    //Remove moves from HashSet of selectedPiece when piece is unMerged
    public void removeMoves(HashSet<Move> moves)
    {
        this.moves.removeAll(moves);
    }

    //Merge two pieces
    void addMerge(Piece piece)
    {
        mergedWith.add(piece);
    }
}
