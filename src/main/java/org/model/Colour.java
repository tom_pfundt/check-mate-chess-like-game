package org.model;

public enum Colour
{
    GREEN,
    LIGHTBLUE,
    RED,
    ORANGE,
    CRIMSON

}
