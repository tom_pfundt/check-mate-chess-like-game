package org.model;

import java.util.List;
import java.util.stream.IntStream;

public class MoveHistory
{
    private final PlayerID[][] board;
    private final Piece[][] pieces;
    private final int[] scores;

    public MoveHistory(PlayerID[][] board, Piece[][] pieces, int[] scores)
    {
        this.board = new PlayerID[board.length][board[0].length];
        IntStream.range(0, board.length).filter(i -> board[i].length >= 0).forEach(i -> System.arraycopy(board[i], 0, this.board[i], 0, board[i].length));

        //Initialise and copy the chessboard pieces into the move history pieces
        this.pieces = new Piece[pieces.length][pieces[0].length];
        deepCopyPieces(pieces);

        this.scores = new int[scores.length];
        IntStream.range(0, scores.length).forEach(i -> System.arraycopy(scores, 0, this.scores, 0, scores.length));
    }

    private void deepCopyPieces(Piece[][] pieces)
    {
        for (int i = 0; i < pieces.length; i++)
        {
            for (int j = 0; j < pieces[i].length; j++)
            {
                if (pieces[i][j] != null)
                {
                    if (pieces[i][j] instanceof Rook)
                    {
                        this.pieces[i][j] = new Rook(pieces[i][j].getColour(), i, j, pieces[i][j].getPieceID());
                    }
                    else if (pieces[i][j] instanceof Knight)
                    {
                        this.pieces[i][j] = new Knight(pieces[i][j].getColour(), i, j, pieces[i][j].getPieceID());
                    }
                    else if (pieces[i][j] instanceof Bishop)
                    {
                        this.pieces[i][j] = new Bishop(pieces[i][j].getColour(), i, j, pieces[i][j].getPieceID());
                    }
                    if (pieces[i][j].isMerged())
                    {
                        List<Piece> mergedWith = pieces[i][j].getMergedWith();
                        this.pieces[i][j].setMerged(true);
                        for (Piece merge : mergedWith)
                        {
                            this.pieces[i][j].addMerge(merge);
                            this.pieces[i][j].addMoves(merge.getMoves());
                        }
                    }
                }
            }
        }
    }

    public PlayerID[][] getBoard()
    {
        return board;
    }

    public Piece[][] getPieces()
    {
        return pieces;
    }

    public int[] getScores()
    {
        return scores;
    }
}
