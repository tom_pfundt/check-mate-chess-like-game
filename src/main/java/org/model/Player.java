package org.model;

public class Player
{
    //Declare variable to capture each individual players username, maximum moves and score
    private final String username;
    private final int maxMoves;
    private int score;

    public Player(String username, int maxMoves, int score)
    {
        this.username = username;
        this.maxMoves = maxMoves;
        this.score = score;
    }

    public String getUsername()
    {
        return username;
    }

    public int getMaxMoves()
    {
        return maxMoves;
    }

    public int getScore()
    {
        return score;
    }

    public void setScore(int score)
    {
        this.score = score;
    }
}
