package org.model;

class Rook extends Piece
{
    //Constructor for Rook
    Rook(PlayerID PlayerColour, int xPosition, int yPosition, String id)
    {
        //Call super Class constructor (Piece)
        super(PlayerColour, xPosition, yPosition, id);

        //Populate Rooks potential moves HashSet
        moves.add(new Move(0, 1));
        moves.add(new Move(0, 2));
        moves.add(new Move(0, -1));
        moves.add(new Move(0, -2));
        moves.add(new Move(1, 0));
        moves.add(new Move(2, 0));
        moves.add(new Move(-1, 0));
        moves.add(new Move(-2, 0));
    }
}
