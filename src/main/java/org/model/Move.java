package org.model;

public class Move
{
    //Declare move x, y coordinate variables
    private final int x;
    private final int y;

    //Constructor for moves
    Move(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

}
