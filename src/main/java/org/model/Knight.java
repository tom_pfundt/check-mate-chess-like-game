package org.model;


public class Knight extends Piece
{

    //Constructor for Knight
    Knight(PlayerID PlayerColour, int xPosition, int yPosition, String id)
    {
        //Call super Class constructor (Piece)
        super(PlayerColour, xPosition, yPosition, id);

        //Populate Knights potential moves HashSet
        moves.add(new Move(-2, -1));
        moves.add(new Move(-2, 1));
        moves.add(new Move(-1, -2));
        moves.add(new Move(-1, 2));
        moves.add(new Move(1, -2));
        moves.add(new Move(1, 2));
        moves.add(new Move(2, -1));
        moves.add(new Move(2, 1));
    }
}
