package org.model;

public enum PlayerID
{
    WHITE,
    BLACK,
    NO_PLAYER
}