package org.model;

import java.util.ArrayList;

public class ChessBoard
{
    //Declare player array
    private Player[] players;

    //Declare Board parameter fields
    private PlayerID[][] board;

    //As the board is a square, board size is used for both height and width
    private final int boardSize;

    //Declare piece array
    private Piece[][] pieces;

    //Declare variable to hold the selected piece each move
    private Piece selectedPiece;

    //Declare players turn members
    private PlayerID playersTurn;

    //Declare move history and data variables
    private ArrayList<MoveHistory> moveHistory;
    private int movesPlayed;
    private int movesRemaining;

    public ChessBoard()
    {
        boardSize = 6;
        // initialize board array to the correct size
        board = new PlayerID[boardSize][boardSize];

        // initialize pieces array to the correct size and then populate with pieces
        pieces = new Piece[boardSize][boardSize];
        initialisePieces();

        //initialise the board[][] integers with their status' - 0 for vacant 1 for white player, 2 for black player
        initialiseBoard();

        //Initialise the selected piece as null
        selectedPiece = null;

        //initialise the player array as 2 elements
        players = new Player[2];

        //Initialise the players turn as player white - white goes first
        playersTurn = PlayerID.WHITE;

        //Initialise moves played as 0
        movesPlayed = 0;

        //Initialise moveHistory as a 3 element array
        moveHistory = new ArrayList<>();

        //initialise first element of moveHistory
        moveHistory.add(new MoveHistory(board, pieces, (new int[]{0, 0})));
    }

    private void initialisePieces()
    {
        //WHITE Pieces
        //Declare individual pieces for each colour in their initial starting position
        pieces[0][0] = new Rook(PlayerID.WHITE, 0, 0, "wR1");
        pieces[1][0] = new Knight(PlayerID.WHITE, 1, 0, "wK1");
        pieces[2][0] = new Bishop(PlayerID.WHITE, 2, 0, "wB1");
        pieces[3][0] = new Bishop(PlayerID.WHITE, 3, 0, "wB2");
        pieces[4][0] = new Knight(PlayerID.WHITE, 4, 0, "wK2");
        pieces[5][0] = new Rook(PlayerID.WHITE, 5, 0, "wR2");
        // BLACK Pieces
        pieces[0][5] = new Rook(PlayerID.BLACK, 0, 5, "bR1");
        pieces[1][5] = new Knight(PlayerID.BLACK, 1, 5, "bK1");
        pieces[2][5] = new Bishop(PlayerID.BLACK, 2, 5, "bB1");
        pieces[3][5] = new Bishop(PlayerID.BLACK, 3, 5, "bB2");
        pieces[4][5] = new Knight(PlayerID.BLACK, 4, 5, "bK2");
        pieces[5][5] = new Rook(PlayerID.BLACK, 5, 5, "bR2");
    }

    private void initialiseBoard()
    {
        //declare and initialise iterator y
        int y = 0;
        while (y < boardSize)
        {
            for (int x = 0; x < boardSize; x++)
            {
                //Declare a value for each of the board cells - 0 for vacant squares, 1 for player white squares,
                // 2 for player black squares
                if (y == 0)
                    board[x][y] = PlayerID.WHITE;
                else if (y == boardSize - 1)
                    board[x][y] = PlayerID.BLACK;
                else
                    board[x][y] = PlayerID.NO_PLAYER;
            }
            y++;
        }
    }

    public int getBoardSize()
    {
        return boardSize;
    }

    public Piece getSelectedPiece()
    {
        return selectedPiece;
    }

    public PlayerID[][] getBoard()
    {
        return board;
    }

    public Player[] getPlayers()
    {
        return players;
    }

    public void setPlayers(Player[] players)
    {
        this.players = players;
    }

    public Piece[][] getPieces()
    {
        return pieces;
    }

    public int getMovesPlayed()
    {
        return movesPlayed;
    }

    public int getMovesRemaining()
    {
        return movesRemaining;
    }

    public PlayerID getPlayersTurn()
    {
        return playersTurn;
    }

    public void setBoard(PlayerID[][] board)
    {
        this.board = board;
    }

    public void setPieces(Piece[][] pieces)
    {
        this.pieces = pieces;
    }

    public void setSelectedPiece(Piece selectedPiece)
    {
        this.selectedPiece = selectedPiece;
    }

    public void setMovesPlayed(int movesPlayed)
    {
        this.movesPlayed = movesPlayed;
    }

    public void setMovesRemaining(int movesRemaining)
    {
        this.movesRemaining = movesRemaining;
    }

    public ArrayList<MoveHistory> getMoveHistory()
    {
        return moveHistory;
    }

    public void setPlayersTurn(PlayerID playersTurn)
    {
        this.playersTurn = playersTurn;
    }

    public void setPiece(int x, int y, Piece piece)
    {
        this.pieces[x][y] = piece;
    }

    //update a specific board square with a new value: 0 for vacant squares, 1 for player white squares 2 for
    // player black squares
    public void updateBoard(int x, int y, PlayerID type)
    {
        this.board[x][y] = type;
    }

    //Initialise the default players when the database connection fails
    public void initialiseDefaultPlayers()
    {
        players[0] = new Player("Player 1", 30, 0);
        players[1] = new Player("Player 2", 30, 0);
    }
}