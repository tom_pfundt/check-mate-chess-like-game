package org.model;


class Bishop extends Piece
{
    //Constructor for Bishop
    Bishop(PlayerID playerColour, int xPosition, int yPosition, String id)
    {
        //Call super Class constructor (Piece)
        super(playerColour, xPosition, yPosition, id);

        //Populate Bishops potential moves HashSet
        moves.add(new Move(-2, -2));
        moves.add(new Move(-2, 2));
        moves.add(new Move(-1, -1));
        moves.add(new Move(-1, 1));
        moves.add(new Move(1, -1));
        moves.add(new Move(1, 1));
        moves.add(new Move(2, -2));
        moves.add(new Move(2, 2));
    }
}
