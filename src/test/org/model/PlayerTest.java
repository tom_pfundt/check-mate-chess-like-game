package org.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest
{
    private Player player;

    @BeforeEach
    void setUp()
    {
        player = new Player("testPlayer", 30, 5);
    }

    @Test
    void getUsername()
    {
        assertEquals("testPlayer", player.getUsername());
    }

    @Test
    void getMaxMoves()
    {
        assertEquals(30, player.getMaxMoves());
    }

    @Test
    void getScore()
    {
        assertEquals(5, player.getScore());
    }

    @Test
    void setScore()
    {
        assertEquals(5, player.getScore());
        player.setScore(10);
        assertEquals(10, player.getScore());
    }
}