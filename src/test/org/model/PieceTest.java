package org.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PieceTest
{
    private Piece whiteRook;
    private Piece whiteBishop;
    private Piece whiteKnight;
    private Piece blackRook;
    private Piece blackBishop;
    private Piece blackKnight;

    @BeforeEach
    void setUp()
    {
        whiteRook = new Rook(PlayerID.WHITE, 5, 3, "wR1");
        whiteBishop = new Bishop(PlayerID.WHITE, 4, 3, "wB1");
        whiteKnight = new Knight(PlayerID.WHITE, 3, 3, "wK1");
        blackRook = new Rook(PlayerID.BLACK, 2, 2, "bR1");
        blackBishop = new Bishop(PlayerID.BLACK, 2, 3, "bB1");
        blackKnight = new Knight(PlayerID.BLACK, 3, 2, "bK1");
    }

    @Test
    void getImageDir()
    {
        assertEquals("file:src/main/resources/PieceImages/RookWHITE.png", whiteRook.getImageDir());
    }

    @Test
    void getMoves()
    {
        assertEquals(8, whiteRook.getMoves().size());

        for (Move move : whiteRook.getMoves())
        {
            if (move.getX() == 0)
            {
                assertTrue(move.getY() == 1 || move.getY() == 2 || move.getY() == -1 || move.getY() == -2);
            }
            else
            {
                assertEquals(0, move.getY());
            }
            if (move.getY() == 0)
            {
                assertTrue(move.getX() == 1 || move.getX() == 2 || move.getX() == -1 || move.getX() == -2);
            }
            else
            {
                assertEquals(0, move.getX());
            }
        }
    }

    @Test
    void getColour()
    {
        assertEquals(PlayerID.WHITE, whiteRook.getColour());
    }

    @Test
    void getxPosition()
    {
        assertEquals(5, whiteRook.getxPosition());
    }

    @Test
    void getyPosition()
    {
        assertEquals(3, whiteRook.getyPosition());
    }

    @Test
    void getPieceID()
    {
        assertEquals("wR1", whiteRook.getPieceID());
    }

    @Test
    void isMerged()
    {
        assertFalse(whiteRook.isMerged());
    }

    @Test
    void setMerged()
    {
        assertFalse(whiteRook.isMerged());
        whiteRook.setMerged(true);
        assertTrue(whiteRook.isMerged());
    }

    @Test
    void getMergedWith()
    {

        whiteRook.addMerge(whiteBishop);
        assertTrue(whiteRook.getMergedWith().contains(whiteBishop));
    }

    @Test
    void getUnMergePiece()
    {

        whiteRook.setUnMergePiece(whiteBishop);
        assertEquals(whiteRook.getUnMergePiece(), whiteBishop);
    }

    @Test
    void setUnMergePiece()
    {

        whiteRook.setUnMergePiece(whiteBishop);
        assertEquals(whiteRook.getUnMergePiece(), whiteBishop);
    }

    @Test
    void isUnMerge()
    {
        assertFalse(whiteRook.isUnMerge());
        whiteRook.setUnMerge(true);
        assertTrue(whiteRook.isUnMerge());
    }

    @Test
    void setUnMerge()
    {
        assertFalse(whiteRook.isUnMerge());
        whiteRook.setUnMerge(true);
        assertTrue(whiteRook.isUnMerge());
    }

    @Test
    void setxPosition()
    {
        assertEquals(5, whiteRook.getxPosition());
        whiteRook.setxPosition(1);
        assertEquals(1, whiteRook.getxPosition());
    }

    @Test
    void setyPosition()
    {
        assertEquals(3, whiteRook.getyPosition());
        whiteRook.setyPosition(1);
        assertEquals(1, whiteRook.getyPosition());
    }

    @Test
    void addMoves()
    {
        assertEquals(8, whiteRook.getMoves().size());
        whiteRook.addMoves(whiteKnight.getMoves());
        assertEquals(16, whiteRook.getMoves().size());

    }

    @Test
    void removeMoves()
    {
        blackRook.addMoves(blackKnight.getMoves());
        assertEquals(16, blackRook.getMoves().size());
        blackRook.removeMoves(blackBishop.getMoves());
        assertEquals(16, blackRook.getMoves().size());
        blackRook.removeMoves(blackKnight.getMoves());
        assertEquals(8, blackRook.getMoves().size());
    }

    @Test
    void addMerge()
    {
        assertTrue(whiteRook.getMergedWith().isEmpty());

        whiteRook.addMerge(whiteBishop);
        assertTrue(whiteRook.getMergedWith().contains(whiteBishop));
    }
}