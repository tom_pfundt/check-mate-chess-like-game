package org.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChessBoardTest
{
    private ChessBoard chessBoard;
    private Piece whiteRook;

    @Test
    void initialiseDefaultPlayers()
    {
        chessBoard.initialiseDefaultPlayers();

        assertEquals("Player 1", chessBoard.getPlayers()[0].getUsername());
        assertEquals(30, chessBoard.getPlayers()[0].getMaxMoves());
        assertEquals(0, chessBoard.getPlayers()[0].getScore());
        assertEquals("Player 2", chessBoard.getPlayers()[1].getUsername());
        assertEquals(30, chessBoard.getPlayers()[1].getMaxMoves());
        assertEquals(0, chessBoard.getPlayers()[1].getScore());
    }

    @BeforeEach
    void setUp()
    {
        chessBoard = new ChessBoard();
        whiteRook = new Rook(PlayerID.WHITE, 5, 3, "wR1");
    }

    @Test
    void selectedPiece()
    {
        assertNull(chessBoard.getSelectedPiece());
        chessBoard.setSelectedPiece(whiteRook);
        assertEquals(whiteRook, chessBoard.getSelectedPiece());
    }


    @Test
    void players()
    {
        assertNull(chessBoard.getPlayers()[0]);
        Player[] players = new Player[2];
        players[0] = new Player("testPlayer", 30, 0);
        chessBoard.setPlayers(players);
        assertEquals("testPlayer", chessBoard.getPlayers()[0].getUsername());
    }

    @Test
    void pieces()
    {
        assertEquals("wR1", chessBoard.getPieces()[0][0].getPieceID());
        assertEquals("wK1", chessBoard.getPieces()[1][0].getPieceID());
        assertEquals("wB1", chessBoard.getPieces()[2][0].getPieceID());
        assertEquals("wB2", chessBoard.getPieces()[3][0].getPieceID());
        assertEquals("wK2", chessBoard.getPieces()[4][0].getPieceID());
        assertEquals("wR2", chessBoard.getPieces()[5][0].getPieceID());
        assertEquals("bR1", chessBoard.getPieces()[0][5].getPieceID());
        assertEquals("bK1", chessBoard.getPieces()[1][5].getPieceID());
        assertEquals("bB1", chessBoard.getPieces()[2][5].getPieceID());
        assertEquals("bB2", chessBoard.getPieces()[3][5].getPieceID());
        assertEquals("bK2", chessBoard.getPieces()[4][5].getPieceID());
        assertEquals("bR2", chessBoard.getPieces()[5][5].getPieceID());

        for (int i = 0; i < chessBoard.getBoardSize(); i++)
        {
            for (int j = 1; j < 5; j++)
            {
                assertNull(chessBoard.getPieces()[i][j]);
            }
        }

        Piece[][] pieces = new Piece[chessBoard.getBoardSize()][chessBoard.getBoardSize()];

        pieces[0][2] = new Rook(PlayerID.WHITE, 0, 0, "wR1");
        pieces[1][2] = new Knight(PlayerID.WHITE, 1, 0, "wK1");
        pieces[2][2] = new Bishop(PlayerID.WHITE, 2, 0, "wB1");
        pieces[3][2] = new Bishop(PlayerID.WHITE, 3, 0, "wB2");
        pieces[4][2] = new Knight(PlayerID.WHITE, 4, 0, "wK2");
        pieces[5][2] = new Rook(PlayerID.WHITE, 5, 0, "wR2");
        pieces[0][3] = new Rook(PlayerID.BLACK, 0, 5, "bR1");
        pieces[1][3] = new Knight(PlayerID.BLACK, 1, 5, "bK1");
        pieces[2][3] = new Bishop(PlayerID.BLACK, 2, 5, "bB1");
        pieces[3][3] = new Bishop(PlayerID.BLACK, 3, 5, "bB2");
        pieces[4][3] = new Knight(PlayerID.BLACK, 4, 5, "bK2");
        pieces[5][3] = new Rook(PlayerID.BLACK, 5, 5, "bR2");

        chessBoard.setPieces(pieces);

        assertEquals("wR1", chessBoard.getPieces()[0][2].getPieceID());
        assertEquals("wK1", chessBoard.getPieces()[1][2].getPieceID());
        assertEquals("wB1", chessBoard.getPieces()[2][2].getPieceID());
        assertEquals("wB2", chessBoard.getPieces()[3][2].getPieceID());
        assertEquals("wK2", chessBoard.getPieces()[4][2].getPieceID());
        assertEquals("wR2", chessBoard.getPieces()[5][2].getPieceID());
        assertEquals("bR1", chessBoard.getPieces()[0][3].getPieceID());
        assertEquals("bK1", chessBoard.getPieces()[1][3].getPieceID());
        assertEquals("bB1", chessBoard.getPieces()[2][3].getPieceID());
        assertEquals("bB2", chessBoard.getPieces()[3][3].getPieceID());
        assertEquals("bK2", chessBoard.getPieces()[4][3].getPieceID());
        assertEquals("bR2", chessBoard.getPieces()[5][3].getPieceID());

        for (int i = 0; i < chessBoard.getBoardSize(); i++)
        {
            for (int j = 0; j < chessBoard.getBoardSize(); j++)
            {
                if (j != 2 && j != 3)
                {
                    assertNull(chessBoard.getPieces()[i][j]);
                }
            }
        }
    }

    @Test
    void movesPlayed()
    {
        assertEquals(0, chessBoard.getMovesPlayed());
        chessBoard.setMovesPlayed(5);
        assertEquals(5, chessBoard.getMovesPlayed());
    }

    @Test
    void movesRemaining()
    {
        assertEquals(0, chessBoard.getMovesRemaining());
        chessBoard.setMovesRemaining(10);
        assertEquals(10, chessBoard.getMovesRemaining());
    }

    @Test
    void playersTurn()
    {
        assertEquals(PlayerID.WHITE, chessBoard.getPlayersTurn());
        chessBoard.setPlayersTurn(PlayerID.BLACK);
        assertEquals(PlayerID.BLACK, chessBoard.getPlayersTurn());
    }

    @Test
    void board()
    {
        //Check current state of board with get board
        for (int i = 0; i < chessBoard.getBoardSize(); i++)
        {
            for (int j = 0; j < chessBoard.getBoardSize(); j++)
            {
                if (j == 0)
                    assertEquals(PlayerID.WHITE, chessBoard.getBoard()[i][j]);
                else if (j == 5)
                    assertEquals(PlayerID.BLACK, chessBoard.getBoard()[i][j]);
                else
                    assertEquals(PlayerID.NO_PLAYER, chessBoard.getBoard()[i][j]);
            }
        }

        //Declare and initialise new board
        PlayerID[][] board = new PlayerID[chessBoard.getBoardSize()][chessBoard.getBoardSize()];

        for (int i = 0; i < chessBoard.getBoardSize(); i++)
        {
            for (int j = 0; j < chessBoard.getBoardSize(); j++)
            {
                board[i][j] = PlayerID.WHITE;
            }
        }

        //Set the board as the new board
        chessBoard.setBoard(board);

        //Check that the chessboard board now equals the new board
        for (int i = 0; i < chessBoard.getBoardSize(); i++)
        {
            for (int j = 0; j < chessBoard.getBoardSize(); j++)
            {
                assertEquals(PlayerID.WHITE, chessBoard.getBoard()[i][j]);
            }
        }
    }

    @Test
    void moveHistory()
    {
        assertEquals(1, chessBoard.getMoveHistory().size());
        int[] scores = new int[]{5,10};

        chessBoard.getMoveHistory().add(new MoveHistory(chessBoard.getBoard(), chessBoard.getPieces(), scores));

        assertEquals(2, chessBoard.getMoveHistory().size());
        assertEquals(10, chessBoard.getMoveHistory().get(chessBoard.getMoveHistory().size()-1).getScores()[1]);
    }

    @Test
    void setPiece()
    {
        chessBoard.setPiece(3, 3, whiteRook);
        assertEquals(whiteRook, chessBoard.getPieces()[3][3]);
    }

    @Test
    void updateBoard()
    {
        assertEquals(PlayerID.NO_PLAYER, chessBoard.getBoard()[2][2]);
        chessBoard.updateBoard(2, 2, PlayerID.WHITE);
        assertEquals(PlayerID.WHITE, chessBoard.getBoard()[2][2]);
    }
}