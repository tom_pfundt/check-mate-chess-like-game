package org.controller;

import org.CheckMate;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.model.PlayerID;
import org.testfx.framework.junit5.ApplicationTest;

import static org.CheckMate.chessBoardScene;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GameControllerTest extends ApplicationTest
{
    private GameController controller;

    //The start method overrides the application start method for the custom stage
    @Override
    public void start(Stage stage)
    {
        //Initialise the players variable on the main application
        CheckMate.gameController = new GameController();
        controller = CheckMate.gameController;
        controller.getChessBoard().initialiseDefaultPlayers();

        //Create a new stage and login scene
        stage = new Stage();
        stage.setTitle("Check Mate");
        stage.setScene(chessBoardScene());
        stage.show();
    }

    @Test
    void testGamePlay()
    {
        assertEquals(0, controller.getChessBoard().getMovesPlayed());
        assertEquals(30, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#wR1");
        assertTrue(controller.getCells()[0][0].isCellHighlighted());
        assertTrue(controller.getCells()[0][1].isCellHighlighted());
        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][0].isCellHighlighted());

        clickOn("#03");
        assertTrue(controller.getCells()[0][0].isCellHighlighted());
        assertTrue(controller.getCells()[0][1].isCellHighlighted());
        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][0].isCellHighlighted());

        clickOn("#02");
        assertEquals("wR1", controller.getChessBoard().getPieces()[0][2].getPieceID());
        assertEquals(PlayerID.NO_PLAYER, controller.getChessBoard().getBoard()[0][0]);
        assertEquals(PlayerID.WHITE, controller.getChessBoard().getBoard()[0][2]);

        assertEquals(1, controller.getChessBoard().getMovesPlayed());
        assertEquals(29, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#bR1");
        assertTrue(controller.getCells()[0][5].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[1][5].isCellHighlighted());

        clickOn("#15");
        assertEquals("bR1", controller.getChessBoard().getPieces()[1][5].getPieceID());
        assertEquals(PlayerID.NO_PLAYER, controller.getChessBoard().getBoard()[0][5]);
        assertEquals(PlayerID.BLACK, controller.getChessBoard().getBoard()[1][5]);
        assertTrue(controller.getChessBoard().getPieces()[1][5].isMerged());

        assertEquals(2, controller.getChessBoard().getMovesPlayed());
        assertEquals(28, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#wR1");
        assertTrue(controller.getCells()[0][0].isCellHighlighted());
        assertTrue(controller.getCells()[0][1].isCellHighlighted());
        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[2][2].isCellHighlighted());

        clickOn("#02");

        checkBoardState();

        clickOn("#wK1");
        assertTrue(controller.getCells()[1][0].isCellHighlighted());
        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[2][2].isCellHighlighted());

        clickOn("#wR1");
        assertEquals("wK1", controller.getChessBoard().getPieces()[0][2].getPieceID());
        assertEquals(PlayerID.NO_PLAYER, controller.getChessBoard().getBoard()[1][0]);
        assertEquals(PlayerID.WHITE, controller.getChessBoard().getBoard()[0][2]);
        assertTrue(controller.getChessBoard().getPieces()[0][2].isMerged());

        assertEquals(3, controller.getChessBoard().getMovesPlayed());
        assertEquals(27, controller.getChessBoard().getMovesRemaining());
        checkBoardState();

        clickOn("#bR1");
        clickOn("Knight");
        assertTrue(controller.getChessBoard().getPieces()[1][5].isUnMerge());
        assertTrue(controller.getCells()[0][5].isCellHighlighted());
        assertTrue(controller.getCells()[1][5].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[2][4].isCellHighlighted());

        clickOn("#05");
        assertFalse(controller.getChessBoard().getPieces()[1][5].isUnMerge());
        assertFalse(controller.getChessBoard().getPieces()[0][5].isUnMerge());
        assertFalse(controller.getChessBoard().getPieces()[1][5].isMerged());
        assertFalse(controller.getChessBoard().getPieces()[0][5].isMerged());

        assertEquals(4, controller.getChessBoard().getMovesPlayed());
        assertEquals(26, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#wK1");
        clickOn("Cancel");
        assertFalse(controller.getChessBoard().getPieces()[0][2].isUnMerge());
        assertEquals(16, controller.getChessBoard().getPieces()[0][2].getMoves().size());
        assertTrue(controller.getChessBoard().getPieces()[0][2].isMerged());
        assertTrue(controller.getCells()[0][0].isCellHighlighted());
        assertTrue(controller.getCells()[0][1].isCellHighlighted());
        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[2][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][0].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());

        clickOn("#14");

        clickOn("OK");

        assertEquals(5, controller.getChessBoard().getMovesPlayed());
        assertEquals(25, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        assertEquals(0, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(0, controller.getChessBoard().getPlayers()[1].getScore());

        clickOn("#bB1");

        assertTrue(controller.getCells()[2][5].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[3][4].isCellHighlighted());
        assertTrue(controller.getCells()[4][3].isCellHighlighted());

        clickOn("#14");

        assertFalse(controller.getChessBoard().getPieces()[1][4].isMerged());
        assertEquals("bB1", controller.getChessBoard().getPieces()[1][4].getPieceID());

        assertEquals(0, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(10, controller.getChessBoard().getPlayers()[1].getScore());

        assertEquals(controller.getChessBoard().getBoardSize(), controller.getChessBoard().getMovesPlayed());
        assertEquals(24, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#gameMenu");
        clickOn("#p1Undo");

        assertEquals(0, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(0, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(5, controller.getChessBoard().getMovesPlayed());
        assertEquals(25, controller.getChessBoard().getMovesRemaining());
        checkBoardState();

        clickOn("#bR1");
        assertTrue(controller.getCells()[2][5].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][3].isCellHighlighted());
        assertTrue(controller.getCells()[0][5].isCellHighlighted());

        clickOn("#13");

        assertEquals(controller.getChessBoard().getBoardSize(), controller.getChessBoard().getMovesPlayed());
        assertEquals(24, controller.getChessBoard().getMovesRemaining());
        checkBoardState();

        clickOn("#wK1");

        clickOn("Cancel");

        assertTrue(controller.getCells()[0][0].isCellHighlighted());
        assertTrue(controller.getCells()[0][1].isCellHighlighted());
        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][0].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[2][1].isCellHighlighted());
        assertTrue(controller.getCells()[2][2].isCellHighlighted());
        assertTrue(controller.getCells()[2][3].isCellHighlighted());

        clickOn("#04");

        assertEquals(7, controller.getChessBoard().getMovesPlayed());
        assertEquals(23, controller.getChessBoard().getMovesRemaining());

        clickOn("#bR1");

        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[1][1].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][3].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][5].isCellHighlighted());
        assertTrue(controller.getCells()[2][3].isCellHighlighted());
        assertTrue(controller.getCells()[3][3].isCellHighlighted());

        clickOn("#15");

        assertEquals(8, controller.getChessBoard().getMovesPlayed());
        assertEquals(22, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#wK1");

        clickOn("Cancel");

        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[0][5].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[2][3].isCellHighlighted());
        assertTrue(controller.getCells()[2][4].isCellHighlighted());
        assertTrue(controller.getCells()[2][5].isCellHighlighted());

        clickOn("#05");

        clickOn("OK");

        checkBoardState();

        assertEquals(5, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(0, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(9, controller.getChessBoard().getMovesPlayed());
        assertEquals(21, controller.getChessBoard().getMovesRemaining());

        clickOn("#gameMenu");
        clickOn("#p2Undo");

        assertEquals(0, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(0, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(8, controller.getChessBoard().getMovesPlayed());
        assertEquals(22, controller.getChessBoard().getMovesRemaining());

        clickOn("#wR2");

        assertTrue(controller.getCells()[4][0].isCellHighlighted());
        assertTrue(controller.getCells()[5][0].isCellHighlighted());
        assertTrue(controller.getCells()[5][1].isCellHighlighted());
        assertTrue(controller.getCells()[5][2].isCellHighlighted());

        clickOn("#wK2");

        assertTrue(controller.getChessBoard().getPieces()[4][0].isMerged());
        assertEquals(9, controller.getChessBoard().getMovesPlayed());
        assertEquals(21, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#bR2");

        assertTrue(controller.getCells()[4][5].isCellHighlighted());
        assertTrue(controller.getCells()[5][3].isCellHighlighted());
        assertTrue(controller.getCells()[5][4].isCellHighlighted());
        assertTrue(controller.getCells()[5][4].isCellHighlighted());

        clickOn("#bK2");

        assertTrue(controller.getChessBoard().getPieces()[4][5].isMerged());
        assertEquals(10, controller.getChessBoard().getMovesPlayed());
        assertEquals(20, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#wR2");

        clickOn("Cancel");

        assertTrue(controller.getCells()[2][1].isCellHighlighted());
        assertTrue(controller.getCells()[3][0].isCellHighlighted());
        assertTrue(controller.getCells()[3][2].isCellHighlighted());
        assertTrue(controller.getCells()[4][0].isCellHighlighted());
        assertTrue(controller.getCells()[4][1].isCellHighlighted());
        assertTrue(controller.getCells()[4][2].isCellHighlighted());
        assertTrue(controller.getCells()[5][0].isCellHighlighted());
        assertTrue(controller.getCells()[5][2].isCellHighlighted());

        clickOn("#wB2");

        assertTrue(controller.getChessBoard().getPieces()[3][0].isMerged());
        assertEquals(24, controller.getChessBoard().getPieces()[3][0].getMoves().size());
        assertEquals(11, controller.getChessBoard().getMovesPlayed());
        assertEquals(19, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#bR2");

        clickOn("Cancel");

        assertTrue(controller.getCells()[2][4].isCellHighlighted());
        assertTrue(controller.getCells()[3][3].isCellHighlighted());
        assertTrue(controller.getCells()[3][5].isCellHighlighted());
        assertTrue(controller.getCells()[4][3].isCellHighlighted());
        assertTrue(controller.getCells()[4][4].isCellHighlighted());
        assertTrue(controller.getCells()[4][5].isCellHighlighted());
        assertTrue(controller.getCells()[5][3].isCellHighlighted());
        assertTrue(controller.getCells()[5][5].isCellHighlighted());

        clickOn("#bB2");

        assertTrue(controller.getChessBoard().getPieces()[3][5].isMerged());
        assertEquals(24, controller.getChessBoard().getPieces()[3][5].getMoves().size());
        assertEquals(12, controller.getChessBoard().getMovesPlayed());
        assertEquals(18, controller.getChessBoard().getMovesRemaining());

        checkBoardState();
        clickOn("#wK1");

        clickOn("Cancel");

        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[0][5].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[2][3].isCellHighlighted());
        assertTrue(controller.getCells()[2][4].isCellHighlighted());
        assertTrue(controller.getCells()[2][5].isCellHighlighted());

        clickOn("#12");

        clickOn("OK");

        assertEquals(13, controller.getChessBoard().getMovesPlayed());
        assertEquals(17, controller.getChessBoard().getMovesRemaining());

        checkBoardState();
        clickOn("#bR1");

        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][3].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][5].isCellHighlighted());
        assertTrue(controller.getCells()[2][3].isCellHighlighted());
        assertTrue(controller.getCells()[3][3].isCellHighlighted());

        clickOn("#12");

        clickOn("OK");

        assertEquals(0, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(10, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(14, controller.getChessBoard().getMovesPlayed());
        assertEquals(16, controller.getChessBoard().getMovesRemaining());

        checkBoardState();
        clickOn("#wR2");

        clickOn("Cancel");

        assertTrue(controller.getCells()[1][1].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[2][1].isCellHighlighted());
        assertTrue(controller.getCells()[2][2].isCellHighlighted());
        assertTrue(controller.getCells()[3][0].isCellHighlighted());
        assertTrue(controller.getCells()[3][1].isCellHighlighted());
        assertTrue(controller.getCells()[3][2].isCellHighlighted());
        assertTrue(controller.getCells()[4][0].isCellHighlighted());
        assertTrue(controller.getCells()[4][1].isCellHighlighted());
        assertTrue(controller.getCells()[4][2].isCellHighlighted());
        assertTrue(controller.getCells()[5][0].isCellHighlighted());
        assertTrue(controller.getCells()[5][1].isCellHighlighted());
        assertTrue(controller.getCells()[5][2].isCellHighlighted());

        clickOn("#12");

        assertEquals(5, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(10, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(15, controller.getChessBoard().getMovesPlayed());
        assertEquals(15, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#bK1");

        assertTrue(controller.getCells()[0][5].isCellHighlighted());
        assertTrue(controller.getCells()[1][3].isCellHighlighted());
        assertTrue(controller.getCells()[2][4].isCellHighlighted());

        clickOn("#24");

        clickOn("OK");

        assertEquals(5, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(10, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(16, controller.getChessBoard().getMovesPlayed());
        assertEquals(14, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#wR2");

        clickOn("Cancel");

        assertTrue(controller.getCells()[0][0].isCellHighlighted());
        assertTrue(controller.getCells()[0][1].isCellHighlighted());
        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][0].isCellHighlighted());
        assertTrue(controller.getCells()[1][1].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][3].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[2][1].isCellHighlighted());
        assertTrue(controller.getCells()[2][2].isCellHighlighted());
        assertTrue(controller.getCells()[2][3].isCellHighlighted());
        assertTrue(controller.getCells()[2][4].isCellHighlighted());
        assertTrue(controller.getCells()[3][0].isCellHighlighted());
        assertTrue(controller.getCells()[3][1].isCellHighlighted());
        assertTrue(controller.getCells()[3][2].isCellHighlighted());
        assertTrue(controller.getCells()[3][3].isCellHighlighted());
        assertTrue(controller.getCells()[3][4].isCellHighlighted());

        clickOn("#24");

        clickOn("Cancel");

        clickOn("#24");

        clickOn("OK");

        assertEquals(10, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(10, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(17, controller.getChessBoard().getMovesPlayed());
        assertEquals(13, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#bR2");

        clickOn("Cancel");

        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[2][3].isCellHighlighted());
        assertTrue(controller.getCells()[2][4].isCellHighlighted());
        assertTrue(controller.getCells()[3][3].isCellHighlighted());
        assertTrue(controller.getCells()[3][4].isCellHighlighted());
        assertTrue(controller.getCells()[3][5].isCellHighlighted());
        assertTrue(controller.getCells()[4][3].isCellHighlighted());
        assertTrue(controller.getCells()[4][4].isCellHighlighted());
        assertTrue(controller.getCells()[4][5].isCellHighlighted());
        assertTrue(controller.getCells()[5][3].isCellHighlighted());
        assertTrue(controller.getCells()[5][4].isCellHighlighted());
        assertTrue(controller.getCells()[5][5].isCellHighlighted());

        clickOn("#24");

        assertEquals(10, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(25, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(18, controller.getChessBoard().getMovesPlayed());
        assertEquals(12, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#wB1");

        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][1].isCellHighlighted());
        assertTrue(controller.getCells()[2][0].isCellHighlighted());
        assertTrue(controller.getCells()[3][1].isCellHighlighted());
        assertTrue(controller.getCells()[4][2].isCellHighlighted());

        clickOn("#42");

        clickOn("OK");

        assertEquals(10, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(25, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(19, controller.getChessBoard().getMovesPlayed());
        assertEquals(11, controller.getChessBoard().getMovesRemaining());

        checkBoardState();

        clickOn("#bR2");

        clickOn("Cancel");

        assertTrue(controller.getCells()[0][2].isCellHighlighted());
        assertTrue(controller.getCells()[0][3].isCellHighlighted());
        assertTrue(controller.getCells()[0][4].isCellHighlighted());
        assertTrue(controller.getCells()[0][5].isCellHighlighted());
        assertTrue(controller.getCells()[1][2].isCellHighlighted());
        assertTrue(controller.getCells()[1][3].isCellHighlighted());
        assertTrue(controller.getCells()[1][4].isCellHighlighted());
        assertTrue(controller.getCells()[1][5].isCellHighlighted());
        assertTrue(controller.getCells()[2][2].isCellHighlighted());
        assertTrue(controller.getCells()[2][3].isCellHighlighted());
        assertTrue(controller.getCells()[2][4].isCellHighlighted());
        assertTrue(controller.getCells()[3][2].isCellHighlighted());
        assertTrue(controller.getCells()[3][3].isCellHighlighted());
        assertTrue(controller.getCells()[3][4].isCellHighlighted());
        assertTrue(controller.getCells()[3][5].isCellHighlighted());
        assertTrue(controller.getCells()[4][2].isCellHighlighted());
        assertTrue(controller.getCells()[4][3].isCellHighlighted());
        assertTrue(controller.getCells()[4][4].isCellHighlighted());
        assertTrue(controller.getCells()[4][5].isCellHighlighted());

        clickOn("#42");

        clickOn("OK");

        assertEquals(10, controller.getChessBoard().getPlayers()[0].getScore());
        assertEquals(30, controller.getChessBoard().getPlayers()[1].getScore());
        assertEquals(20, controller.getChessBoard().getMovesPlayed());
        assertEquals(10, controller.getChessBoard().getMovesRemaining());

        checkBoardState();
        press(KeyCode.ENTER);

        clickOn("Quit");
    }

    void checkBoardState()
    {
        for (int i = 0; i < controller.getChessBoard().getBoardSize(); i++)
        {
            for (int j = 0; j < controller.getChessBoard().getBoardSize(); j++)
            {
                assertFalse(controller.getCells()[i][j].isCellHighlighted());
            }
        }
        for (int i = 0; i < controller.getChessBoard().getBoardSize(); i++)
        {
            for (int j = 0; j < controller.getChessBoard().getBoardSize(); j++)
            {
                if (controller.getChessBoard().getPieces()[i][j] == null)
                {
                    assertEquals(PlayerID.NO_PLAYER, controller.getChessBoard().getBoard()[i][j]);
                }
                else if (controller.getChessBoard().getPieces()[i][j].getPieceID().contains("w"))
                {

                    assertEquals(PlayerID.WHITE, controller.getChessBoard().getBoard()[i][j]);
                }
                else
                {
                    assertEquals(PlayerID.BLACK, controller.getChessBoard().getBoard()[i][j]);
                }
            }
        }
    }
}