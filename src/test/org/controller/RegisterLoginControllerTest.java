package org.controller;

import org.CheckMate;
import org.model.Player;
import ch.vorburger.exec.ManagedProcessException;
import ch.vorburger.mariadb4j.DB;
import ch.vorburger.mariadb4j.DBConfiguration;
import ch.vorburger.mariadb4j.DBConfigurationBuilder;
import com.sun.javafx.robot.impl.FXRobotHelper;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import mockit.Deencapsulation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testfx.framework.junit5.ApplicationTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.CheckMate.loginScene;
import static org.controller.DatabaseController.createTable;
import static org.junit.jupiter.api.Assertions.*;

class RegisterLoginControllerTest extends ApplicationTest
{
    private Player[] players;

    //Declare private variables for use throughout test cases
    private static Connection testConnection;

    //Set up the Embedded JDBC connection prior to the tests being run
    @BeforeAll
    static void setUpClass() throws ManagedProcessException, SQLException
    {
        //Declare and initialise a configuration  for an embedded database builder
        DBConfigurationBuilder configBuilder = DBConfigurationBuilder.newBuilder();

        //Set the port configuration to 0 so that it detects a free port
        configBuilder.setPort(0);

        //Build the configuration
        DBConfiguration config = configBuilder.build();

        //Initialise embedded database with the built configuration
        DB db = DB.newEmbeddedDB(config);

        //Start the embedded database
        db.start();

        //Create a test database
        db.createDB("sef");

        //Get a testConnection to the test database
        testConnection = DriverManager.getConnection(configBuilder.getURL("sef"), "root", "");

        //Set the field "testConnection" to mimic the field "connection" from the DatabaseController class
        Deencapsulation.setField(DatabaseController.class, "connection", testConnection);
    }

    //Close the embedded connection once all tests are run
    @AfterAll
    static void tearDownClass() throws SQLException
    {
        //Close the connection
        testConnection.close();
    }


    //The start method overrides the application start method for the custom stage
    @Override
    public void start(Stage stage)
    {
        //Create a new stage and login scene
        stage = new Stage();
        stage.setTitle("Check Mate");
        stage.setScene(loginScene());
        stage.show();
    }

    @BeforeEach
    void setUp() throws SQLException
    {
        //Prepare and execute an SQL statement to clear the database of the table "checkmate" if it exists
        testConnection.prepareStatement("drop table IF EXISTS sef.checkmate").execute();

        createTable();

        PreparedStatement prepStmt = testConnection.prepareStatement("INSERT into sef.checkmate Values(?, SHA(?));");

        //insert a player into the new table on the embedded database
        //Use binding to protect from SQL injection
        prepStmt.setString(1, "existingPlayer");
        prepStmt.setString(2, "existingPlayer");

        prepStmt.executeQuery();

        //Initialise the players variable on the main application
        CheckMate.gameController = new GameController();
        players = CheckMate.gameController.getChessBoard().getPlayers();
    }

    //This test is to ensure a nonExistingPlayer can register and be automatically logged in as player 1 (players[0])
    @Test
    void nonExistingPlayer1Register()
    {
        //Automatically populate login, password and maximum moves text fields with predefined values
        clickOn("#username");
        write("nonExistingPlayer");
        clickOn("#password");
        write("nonExistingPlayer");
        clickOn("#maxMoves");
        write("20");
        //Click the register button
        clickOn("#register");
        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 1 has been registered and automatically logged in
        assertEquals(players[0].getUsername(), "nonExistingPlayer");
    }

    //This test is to ensure a nonExistingPlayer can register and be automatically logged in as player 2 (players[1])
    @Test
    void nonExistingPlayer2Register()
    {
        //Pre populate player 1
        players[0] = new Player("Player1", 20, 0);

        //Automatically populate login, password and maximum moves text fields with predefined values
        clickOn("#username");
        write("nonExistingPlayer");
        clickOn("#password");
        write("nonExistingPlayer");
        clickOn("#maxMoves");
        write("20");
        //Click the register button
        clickOn("#register");
        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 2 has been registered and automatically logged in
        assertEquals(players[1].getUsername(), "nonExistingPlayer");
    }

    //This test is to ensure an existingPlayer cannot register as the username is taken
    @Test
    void existingPlayerRegister()
    {
        //Automatically populate login, password and maximum moves text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#password");
        write("existingPlayer");
        clickOn("#maxMoves");
        write("20");
        //Click the register button
        clickOn("#register");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that player 1 is null - no player has been registered or automatically logged in
        assertNull(players[0]);
    }

    //This test is to ensure a nonExistingPlayer cannot login as the username does not exist
    @Test
    void nonExistingPlayerLogin()
    {
        //Automatically populate login, password and maximum moves text fields with predefined values
        clickOn("#username");
        write("nonExistingPlayer");
        clickOn("#password");
        write("nonExistingPlayer");
        clickOn("#maxMoves");
        write("20");

        //Click on the login button
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 1 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure an existingPlayer can login as player 1 (players[0])
    @Test
    void existingPlayer1Login()
    {
        //Automatically populate login, password and maximum moves text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#password");
        write("existingPlayer");
        clickOn("#maxMoves");
        write("20");

        //click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 1 has been logged in
        assertEquals(players[0].getUsername(), "existingPlayer");
    }

    //This test is to ensure an existingPlayer can login as player 1 (players[0])
    @Test
    void existingPlayer2Login()
    {
        //Pre populate player 1
        players[0] = new Player("Player1", 20, 0);

        //Automatically populate login, password and maximum moves text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#password");
        write("existingPlayer");
        clickOn("#maxMoves");
        write("20");

        //click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 2 has been logged in
        assertEquals(players[1].getUsername(), "existingPlayer");
    }

    //This test is to ensure an existingPlayer cannot login as both players
    @Test
    void existingPlayerAlreadyLoggedIn()
    {
        //Pre populate player 1

        players[0] = new Player("existingPlayer", 20, 0);

        //Automatically populate login, password and maximum moves text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#password");
        write("existingPlayer");
        clickOn("#maxMoves");
        write("20");

        //click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 1 (prepopulated) is still logged in
        assertEquals(players[0].getUsername(), "existingPlayer");

        //Assert that the player 2 has not been logged in - player 2 = null
        assertNull(players[1]);
    }

    //This test is to ensure that a player cannot register with an empty username
    @Test
    void emptyUsernameRegister()
    {
        //Automatically populate password and maximum moves text fields with predefined values
        clickOn("#password");
        write("nonExistingPlayer");
        clickOn("#maxMoves");
        write("20");

        //Click on Register
        clickOn("#register");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot register with an empty password field
    @Test
    void emptyPasswordRegister()
    {
        //Automatically populate login and maximum moves text fields with predefined values
        clickOn("#username");
        write("nonExistingPlayer");
        clickOn("#maxMoves");
        write("20");

        //Click on Register
        clickOn("#register");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot register with an empty maxMoves amount
    @Test
    void emptyMovesRegister()
    {
        //Automatically populate login and password text fields with predefined values
        clickOn("#username");
        write("nonExistingPlayer");
        clickOn("#password");
        write("nonExistingPlayer");

        //Click on register
        clickOn("#register");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot register with an 0 or less maxMoves amount
    @Test
    void zeroMovesOrLessRegister()
    {
        //Automatically populate login and password text fields with predefined values
        clickOn("#username");
        write("nonExistingPlayer");
        clickOn("#password");
        write("nonExistingPlayer");
        clickOn("#maxMoves");
        write("0");

        //Click on register
        clickOn("#register");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot login with an empty username
    @Test
    void emptyUsernameLogin()
    {
        //Automatically populate password and max moves text fields with predefined values
        clickOn("#password");
        write("existingPlayer");
        clickOn("#maxMoves");
        write("20");

        //Click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot login with an empty password field
    @Test
    void emptyPasswordLogin()
    {
        //Automatically populate login and max moves text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#maxMoves");
        write("20");

        //Click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot login with an empty maxMoves amount
    @Test
    void emptyMovesLogin()
    {
        //Automatically populate login and password text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#password");
        write("existingPlayer");

        //Click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot login with an 0 or less maxMoves amount
    @Test
    void zeroMovesOrLessLogin()
    {
        //Automatically populate login and password text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#password");
        write("existingPlayer");
        clickOn("#maxMoves");
        write("0");

        //Click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot login if they enter the wrong data type into max moves (i.e. letters)
    @Test
    void movesIncorrectDataRegister()
    {
        //Automatically populate login, password and max moves text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#password");
        write("existingPlayer");
        clickOn("#maxMoves");
        write("WrongData");

        //Click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to ensure that a player cannot login if they enter the wrong data type into max moves (i.e. letters)
    @Test
    void movesIncorrectDataLogin()
    {
        //Automatically populate login, password and max moves text fields with predefined values
        clickOn("#username");
        write("existingPlayer");
        clickOn("#password");
        write("existingPlayer");

        //Click on login
        clickOn("#login");

        //Press enter to acknowledge the alert popup
        press(KeyCode.ENTER);

        //Assert that the player 0 has not been logged in
        assertNull(players[0]);
    }

    //This test is to test the functionality of the exit button
    @Test
    void exitTest()
    {
        //Click on exit
        clickOn("#fileMenu");
        clickOn("#exit");

        //Assert that there are no stages open
        assertEquals(FXRobotHelper.getStages().size(), 0);
    }
}